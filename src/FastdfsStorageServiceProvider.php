<?php

namespace Wucanheng\LaravelFilesystem\Fastdfs;

use Illuminate\Support\ServiceProvider;
use League\Flysystem\Filesystem;
use Wucanheng\Flysystem\Fastdfs\FastdfsAdapter;

class FastdfsStorageServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        app('filesystem')->extend('fastdfs', function ($app, $config) {

            $adapter = new FastdfsAdapter();

            $filesystem = new Filesystem($adapter);

            return $filesystem;
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
